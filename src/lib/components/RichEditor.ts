import type EditorJS from "@editorjs/editorjs";
import type { EditorConfig, OutputData } from "@editorjs/editorjs";
import { writable } from "svelte/store";

type StoreAPI = {
    save: () => Promise<void>,
    clear: () => Promise<void>,
    render: (data: OutputData) => Promise<void>,
    instance: EditorJS
};

export type CreateEditorAction = () => Promise<(node: HTMLElement) => { destroy: () => void }>;
export type ComponentEditorConfig = Omit<EditorConfig, "holder" | "holderId">;

export function createEditor(configuration: ComponentEditorConfig = {}) {
    const initialData : OutputData = configuration.data ?? {
        time: new Date().getTime(),
        blocks: [],
    };

    const storeData = writable(initialData);
    const storeAPI  = writable<StoreAPI | null>(null);

    let internalInstance: EditorJS | undefined;

    const setDataAndUpdateEditor = (data: OutputData) => {
        storeData.update(oldData => ({ ...oldData, ...data }));
        internalInstance!.render(data);
    };

    async function editor() {
        if (typeof window === "undefined") return;
        const EditorJS = (await import("@editorjs/editorjs")).default;

        return (node: HTMLElement) => {
            const instance = new EditorJS({
                ...configuration,
                holder: node,
            });

            internalInstance = instance;

            const save = async () => {
                const data = await instance.save();
                storeData.set(data);
            };

            const clear = async () => {
                instance.clear();
                storeData.update((data) => ({
                    ...data,
                    time: new Date().getTime(),
                    blocks: [],
                }));
            };

            const render = async (data: OutputData) => await instance.render(data);

            storeAPI.set({ save, clear, render, instance });

            return {
                destroy() {
                    internalInstance?.destroy();
                }
            };
        };
    }

    return {
        api: storeAPI,
        action: editor as CreateEditorAction,
        data: {
            subscribe: storeData.subscribe,
            set: setDataAndUpdateEditor,
            update: storeData.update
        },
    };
}
