import { parseCookies } from "$lib/utils";
import { getUserFromToken, getUserVotes } from "$lib/auth/db";
import type { Handle, GetSession } from "@sveltejs/kit";

export const handle : Handle = async function({ event, resolve }) {
    let cookies = null;

    if (event.request.headers.get("cookie") != null)
        cookies = parseCookies(event.request.headers.get("cookie")!);

    if (cookies && cookies.auth_token)
        try {
            event.locals.user = await getUserFromToken(cookies.auth_token);
            event.locals.user!.votes = await getUserVotes(event.locals.user!.id);
            event.locals.token = cookies.auth_token;
        } catch (e) {
            event.locals.user = null;
            event.locals.token = null;
        }

    return await resolve(event);
}

export const getSession : GetSession = function(event) {
    const user = event.locals.token ? {
        token:         event.locals.token,
        ...event.locals.user!
    } : null;

    return {
        isAuthenticated: !!user,
        user,
    };
};
