CREATE MIGRATION m1wcacycbmpml3znjtzuynlaw57jfnq4652zozbz6d5ylgxgwoo4hq
    ONTO m1dtcfiaes6lbtk7bzv3cgnh3cozjnffxknimiw2o5szaov2wj6ekq
{
  ALTER TYPE default::User {
      CREATE REQUIRED PROPERTY password_hash -> std::str {
          SET REQUIRED USING ('INVALID_PASSWORD_HASH');
      };
  };
};
