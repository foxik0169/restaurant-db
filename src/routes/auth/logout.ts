import type { RequestHandler } from "./__types/logout";

export const GET : RequestHandler = async function get() {
    return {
        status: 302,
        headers: {
            "location": "/",
            "set-cookie": "auth_token=null; path=/;"
        }
    };
}

