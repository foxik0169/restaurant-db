import { getUserVotes } from "$lib/auth/db";
import type { RequestHandler } from "./__types/me";

export const GET : RequestHandler = async function (event) {
    if (!event.locals.user)
        return { status: 401 };

    try {
        return { status: 200, body: await getUserVotes(event.locals.user!.id) };
    } catch(e) {
        return { status: 400, body: e as Error };
    }
};

