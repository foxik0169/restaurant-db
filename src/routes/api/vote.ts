import type { RequestHandler } from "./__types/vote";
import * as zod from "zod";
import { vote, unvote } from "$lib/vote/db";

const schema = zod.object({
    id: zod.string(),
    type: zod.literal("story").or(zod.literal("restaurant"))
});

export const POST : RequestHandler = async function (event) {
    const request = await event.request.json();
    const input = await schema.safeParseAsync(request);

    if (!event.locals.token)
        return { status: 401 };

    if (!input.success)
        return { status: 400 };

    await vote(event.locals.user!.id, input.data.id, input.data.type);

    return { status: 200 };
};

export const DELETE : RequestHandler = async function (event) {
    const request = await event.request.json();
    const input = await schema.safeParseAsync(request);

    if (!event.locals.token)
        return { status: 401 };

    if (!input.success)
        return { status: 400 };

    await unvote(event.locals.user!.id, input.data.id, input.data.type);

    return { status: 200 };
}
