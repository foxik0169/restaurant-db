CREATE MIGRATION m1dtcfiaes6lbtk7bzv3cgnh3cozjnffxknimiw2o5szaov2wj6ekq
    ONTO m1q3w5tkceaynfthmuclbo6q2yxaffpmdf2d6euyaqdan6lwnkrfia
{
  ALTER TYPE default::User {
      ALTER PROPERTY user_name {
          CREATE CONSTRAINT std::exclusive;
      };
  };
};
