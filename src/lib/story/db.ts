import e from "../../../dbschema/edgeql-js";
import type { CreateInput } from "./schema";
import client from "$lib/dbclient";

export async function createStory(input: CreateInput, restaurant_id: string, user_id: string) {
    const query = e.insert(e.Story, {
        restaurant: e.select(e.Restaurant, restaurant => ({
            filter: e.op(restaurant.id, "=", e.uuid(restaurant_id)),
            limit: 1
        })),
        user: e.select(e.User, user => ({
            filter: e.op(user.id, "=", e.uuid(user_id))
        })),
        rich_content: input.rich_content,
        happend_at: input.happend_at,
    });

    return await query.run(client);
}

