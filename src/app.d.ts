/// <reference types="@sveltejs/kit" />

interface LocalUser { /* @todo Get rid of this and use type inference. */
    token?: string,
    id: string,
    user_name: string,
    first_name: string,
    last_name: string,
    registered_at: Date,
    votes?: {
        restaurants: Array<string>,
        stories: Array<string>,
        vote_count: number
    }
}

declare namespace App {
    interface Locals {
        token: string | null,
        user: LocalUser | null,
    }
    // interface Platform {}
    interface Session {
        isAuthenticated: bool,
        user: LocalUser | null
    }
    // interface Stuff {}
}

