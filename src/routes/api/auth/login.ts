import { passwordCompare, createToken } from "$lib/auth";
import { getUser } from "$lib/auth/db";
import type { RequestHandler } from "./__types/login";
import { login as schema } from "$lib/auth/schema";

export const POST : RequestHandler = async function (event) {
    const request = await event.request.json();

    try {
        const input = await schema.parseAsync(request);
        const result = await getUser(input.user_name, true);

        if (!result)
            return { status: 401, body: "User not found." };

        if (!await passwordCompare(input.password, result.password_hash!))
            return { status: 401, body: "Invalid password." };

        const token = await createToken(request.user_name);

        return {
            status: 200,
            body: {
                token,
                id:            result.id,
                user_name:     result.user_name,
                first_name:    result.first_name,
                last_name:     result.last_name,
                registered_at: result.registered_at.toUTCString(),
            },
            headers: {
                "set-cookie": `auth_token=${token}; path=/;`
            }
        };
    } catch (e) {
        return { status: 400, body: e as Error };
    }
};

