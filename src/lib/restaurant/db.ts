import e from "../../../dbschema/edgeql-js";
import type { CreateInput, ListInput } from "./schema";
import client from "$lib/dbclient";

export type GetRestaurantReturnType = ReturnType<typeof getRestaurant>;
export type GetRestaurantsReturnType = ReturnType<typeof getRestaurants>;
export type CreateRestaurantReturnType = ReturnType<typeof createRestaurant>;

export async function getRestaurant(id: string) {
    const query = e.select(e.Restaurant, restaurant => ({
        id: true,
        name: true,
        location: true,
        category: true,
        registered_at: true,
        registered_by: true,
        limit: 1,
        filter: e.op(restaurant.id, "=", e.uuid(id))
    }));

    return query.run(client);
}

export async function getRestaurants(input: ListInput) {
    const query = e.select(e.Restaurant, restaurant => ({
        id: true,
        name: true,
        category: true,
        location: true,
        vote_count: e.count(restaurant.votes),
        registered_at: true,
        registered_by: () => ({
            first_name: true,
            last_name: true,
        }),
        stories: e.select(e.Story, story => ({
            id: true,
            rich_content: true,
            happend_at: true,
            user: () => ({
                first_name: true,
                last_name: true
            }),
            vote_count: e.count(story.votes),
            filter: e.op(story.restaurant, "=", restaurant),
            order_by: {
                expression: e.count(story.votes),
                direction: e.DESC,
                empty: e.EMPTY_LAST
            }
        })),
        filter: input.user_name ?
            e.op(restaurant.registered_by.user_name, "=", input.user_name) :
            (input.user_id ?
                e.op(restaurant.registered_by.id, "=", e.uuid(input.user_id)) :
                undefined),
        order_by: {
            expression: e.count(restaurant.votes),
            direction: e.DESC,
            empty: e.EMPTY_LAST
        }
    }));

    return await query.run(client);
}

export async function createRestaurant(input: CreateInput, user_id: string) {
    const query = e.insert(e.Restaurant, {
        name: input.name,
        category: input.category,
        location: { latitude: input.latitude, longitude: input.longitude },
        registered_at: new Date(),
        registered_by: e.select(e.User, user => ({
            filter: e.op(user.id, "=", e.uuid(user_id))
        }))
    });

    return await query.run(client);
}
