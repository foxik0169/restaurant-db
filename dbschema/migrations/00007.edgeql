CREATE MIGRATION m1w6xzdj2zg4mojmawenee73eeegiz2xfvmoxt55hffy4g3k5ioruq
    ONTO m1unleqs5wectawrv7qs2cjdjjwnbf2ipqmukjlmrv7vq76kcypkka
{
  ALTER TYPE default::Vote {
      DROP LINK vote_for;
  };
  ALTER TYPE default::Vote {
      CREATE LINK vote_for -> default::HasVotes;
  };
  ALTER TYPE default::HasVotes {
      ALTER LINK votes {
          USING (.<vote_for);
          RESET CARDINALITY;
      };
  };
};
