import e, { type Restaurant, type Story } from "../../../dbschema/edgeql-js/index";
import { getTokenPayload, passwordCreate } from "./index";
import client from "$lib/dbclient"

import type { RegisterInput } from "./schema";

export async function getUser(userName: string, return_hash = false) {
    const query = e.select(e.User, user => ({
        id: true,
        user_name: true,
        first_name: true,
        last_name: true,
        password_hash: return_hash,
        registered_at: true,
        filter: e.op(user.user_name, "=", userName)
    }));

    return await query.run(client);
}

export async function getUserFromToken(token: string) {
    const payload = await getTokenPayload(token);
    const user_name = payload.usr;
    return await getUser(user_name);
}

export async function createUser(input: RegisterInput) {
    const query = e.insert(e.User, {
        user_name:     input.user_name,
        first_name:    input.first_name,
        last_name:     input.last_name,
        password_hash: await passwordCreate(input.password),
        registered_at: new Date()
    });

    return await query.run(client);
}

export async function getUserVotes(user_id: string) {
    const query = e.select(e.User, user => ({
        voted_restaurants: e.select(e.select(e.Vote, vote => ({
            vote_for: true,
            filter: e.op(vote, "in", e.Restaurant.votes)
        })), vote => ({
            vote_for: true,
            filter: e.op(vote.vote_by, "=", user)
        })),

        voted_stories: e.select(e.select(e.Vote, vote => ({
            vote_for: true,
            filter: e.op(vote, "in", e.Story.votes)
        })), vote => ({
            vote_for: true,
            filter: e.op(vote.vote_by, "=", user)
        })),

        vote_count: e.count(user["<vote_by"]),
        filter: e.op(user.id, "=", e.uuid(user_id)),
    }));

    const result = await query.run(client);
    const data = {
        restaurants: result!.voted_restaurants.map(i => (<Partial<Restaurant>>i.vote_for)!.id!),
        stories: result!.voted_stories.map(i => (<Partial<Story>>i.vote_for)!.id!),
        vote_count: result!.vote_count
    };

    return data;
}

