import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const { sign, verify } = jwt;

const saltRounds = parseInt(import.meta.env.RESTAURANTDB_SALT_ROUNDS);
const secret     = import.meta.env.RESTAURANTDB_SECRET;

export async function passwordCreate(input: string) {
    return await bcrypt.hash(input, saltRounds);
}

export async function passwordCompare(input: string, hash: string) {
    return await bcrypt.compare(input, hash);
}

export async function createToken(userName: string): Promise<string> {
    const claims = {
        usr: userName
    };

    const options = {
        expiresIn: "12h"
    };

    return await new Promise((resolve, reject) => {
        sign(claims, secret, options, (err, token) => {
            if (err) reject(err);
            else resolve(token as string);
        });
    });
}

export async function getTokenPayload(token: string): Promise<Record<string, string>> {
    const options = {};

    return await new Promise((resolve, reject) => {
        verify(token, secret, options, (err, payload) => {
            if (err) reject(err);
            else resolve(payload as Record<string, string>);
        });
    });
}

