import type { RequestHandler } from "./__types/upload-image";
import crypto from "crypto";
import { promises as fs } from "fs";
import mime from "mime-types";
import path from "path";

export const PUT : RequestHandler = async function (event) {
    const imageData = await event.request.blob();
    const arrayBuffer = await imageData.arrayBuffer();

    const extension = mime.extension(event.request.headers.get("content-type")!);
    const image_id = crypto.randomUUID({});

    const directory = path.join("static", "upload");
    const filePath = path.join(directory, `${image_id}.${extension}`);

    await fs.mkdir(directory, { recursive: true });
    await fs.writeFile(filePath, new Uint8Array(arrayBuffer), {});

    return { status: 200, body: {
        success: 1,
        file: {
            image_id,
            url: `/upload/${image_id}.${extension}`,
        }
    }};
}

