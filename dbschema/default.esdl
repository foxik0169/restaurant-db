module default {

    type Vote {
        required link vote_by -> User;
        required property voted_at -> datetime;
        link vote_for -> HasVotes;
    }

    abstract type HasVotes {
        link votes := .<vote_for;
    }

    type User extending HasVotes {
        required property user_name -> str { constraint exclusive; };
        required property first_name -> str;
        required property last_name -> str;
        required property password_hash -> str;
        required property registered_at -> datetime;
    }

    type Restaurant extending HasVotes {
        required property name -> str;
        required property category -> str;
        property location -> tuple<latitude: float32, longitude: float32>;
        required link registered_by -> User;
        required property registered_at -> datetime;
    }

    type Story extending HasVotes {
        required link restaurant -> Restaurant;
        required link user -> User;
        required property rich_content -> json;
        required property happend_at -> datetime;
    }

}
