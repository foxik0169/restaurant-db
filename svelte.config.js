import adapter from '@sveltejs/adapter-node';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: preprocess({}),

	kit: {
		adapter: adapter(),
        prerender: {
            enabled: false /* currently not working with EdgeQL-js generator */
        }
	}
};

export default config;
