import type { Readable } from "svelte/store";

const contextualHeaders: Record<string, string> = {};
let   internalFetch = fetch;

export const setFetch = async (newFetch: typeof fetch) => internalFetch = newFetch;
export const subscribeSession = (session: Readable<App.Session>) => {
    session.subscribe(store => {
        if (store.isAuthenticated)
            contextualHeaders["authorization"] = `bearer ${store.user!.token}`;
        else
            delete contextualHeaders["authorization"];
    });
};

export default async function request(
    url: string,
    method: "GET" | "POST" | "PUT" | "DELETE",
    payload?: Record<string, unknown>,
    overrides?: Partial<RequestInit>) {

    return await internalFetch(url, {
        credentials: "same-origin",
        headers: {
            "accept": "application/json",
            "content-type": "application/json",
            ...contextualHeaders
        },
        body: JSON.stringify(payload),
        method,
        ...overrides
    });
}

export async function upload(
    url: string,
    payload: File,
    overrides?: Partial<RequestInit>) {

    return await internalFetch(url, {
        credentials: "same-origin",
        headers: {
            "accept": "application/json",
            "content-type": payload.type,
            ...contextualHeaders
        },
        body: payload,
        method: "PUT",
        ...overrides
    });
}
