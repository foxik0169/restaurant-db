CREATE MIGRATION m1unleqs5wectawrv7qs2cjdjjwnbf2ipqmukjlmrv7vq76kcypkka
    ONTO m1afqs643vjircf5gy7k2ymsc47ajxh4f7hrrgj5djfamyj4waokja
{
  ALTER TYPE default::Vote {
      ALTER LINK vote_for {
          USING (.<votes);
      };
  };
};
