import type { RequestHandler } from "./__types/create";
import { create as schema } from "$lib/story/schema";
import { createStory } from "$lib/story/db";

export const POST : RequestHandler = async function(event) {
    if (!event.locals.user)
        return { status: 401 };

    const request = await event.request.json();
    const input = await schema.safeParseAsync(request);

    if (!input.success)
        return { status: 400, body: input.error };

    try {
        const result = await createStory(input.data, event.params.restaurant_id, event.locals.user.id);
        return { status: 200, body: result };
    } catch (e) {
        return { status: 500, body: e as Error };
    }
}
