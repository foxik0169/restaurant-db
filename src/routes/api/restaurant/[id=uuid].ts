import { getRestaurant } from "$lib/restaurant/db";
import type { RequestHandler } from "./__types/[id=uuid]";

export const GET : RequestHandler = async function (event) {
    try {
        const restaurant = await getRestaurant(event.params.id);
        return { status: 200, body: restaurant };
    } catch(e) {
        return { status: 500, body: e as Error };
    }
}
