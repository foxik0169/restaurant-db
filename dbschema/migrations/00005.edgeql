CREATE MIGRATION m1afqs643vjircf5gy7k2ymsc47ajxh4f7hrrgj5djfamyj4waokja
    ONTO m1fkjrh7tfv7xlyervr2ye27tegyhzn2l5h5tazfwowe2wk6wj2pda
{
  ALTER TYPE default::Vote {
      CREATE MULTI LINK vote_for -> default::HasVotes;
  };
};
