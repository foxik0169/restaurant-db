import * as zod from "zod";

export const create = zod.object({
    rich_content: zod.string(),
    happend_at: zod.string().refine(val => !isNaN(Date.parse(val)), "Invalid date").transform(val => new Date(Date.parse(val)))
});

export type CreateInput = zod.infer<typeof create>;
