import type { RequestHandler } from "./__types/register";
import { create as schema } from "$lib/restaurant/schema";
import { createRestaurant } from "$lib/restaurant/db";

export const POST : RequestHandler = async function(event) {
    const request = await event.request.json();
    const input = await schema.safeParseAsync(request);

    if (!input.success)
        return { status: 400, body: input.error.format() };

    try {
        const result = await createRestaurant(input.data, event.locals.user!.id);
        return { status: 200, body: result };
    } catch (e) {
        return { status: 500, body: e as Error };
    }
}
