CREATE MIGRATION m1q3w5tkceaynfthmuclbo6q2yxaffpmdf2d6euyaqdan6lwnkrfia
    ONTO initial
{
  CREATE TYPE default::Vote {
      CREATE REQUIRED PROPERTY voted_at -> std::datetime;
  };
  CREATE ABSTRACT TYPE default::HasVotes {
      CREATE MULTI LINK votes -> default::Vote;
  };
  CREATE TYPE default::User EXTENDING default::HasVotes {
      CREATE REQUIRED PROPERTY first_name -> std::str;
      CREATE REQUIRED PROPERTY last_name -> std::str;
      CREATE REQUIRED PROPERTY registered_at -> std::datetime;
      CREATE REQUIRED PROPERTY user_name -> std::str;
  };
  CREATE TYPE default::Restaurant EXTENDING default::HasVotes {
      CREATE REQUIRED LINK registered_by -> default::User;
      CREATE REQUIRED PROPERTY category -> std::str;
      CREATE PROPERTY location -> tuple<latitude: std::float32, longitude: std::float32>;
      CREATE REQUIRED PROPERTY name -> std::str;
      CREATE REQUIRED PROPERTY registered_at -> std::datetime;
  };
  CREATE TYPE default::Story EXTENDING default::HasVotes {
      CREATE REQUIRED LINK restaurant -> default::Restaurant;
      CREATE REQUIRED LINK user -> default::User;
      CREATE REQUIRED PROPERTY happend_at -> std::datetime;
      CREATE REQUIRED PROPERTY rich_content -> std::json;
  };
};
