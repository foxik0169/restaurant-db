import e from "../../../dbschema/edgeql-js";
import client from "$lib/dbclient";

/* If the surface area of this function expands, consider type inference. */
export async function vote(user_id: string, id: string, type: "story" | "restaurant") {
    let for_query;
    const by_query = e.select(e.User, user => ({ filter: e.op(user.id, "=", e.uuid(user_id)), limit: 1 }));

    switch (type) {
    case "story":
        for_query = e.select(e.Story, story => ({ filter: e.op(story.id, "=", e.uuid(id)), limit: 1 }));
        break;
    case "restaurant":
        for_query = e.select(e.Restaurant, restaurant => ({ filter: e.op(restaurant.id, "=", e.uuid(id)), limit:1 }));
    }

    for_query.assert_single();
    by_query.assert_single();

    const query = e.insert(e.Vote, {
        vote_for: for_query,
        vote_by:  by_query,
        voted_at: new Date()
    });

    return await query.run(client);
}

export async function unvote(user_id: string, id: string, type: "story" | "restaurant") {
    const query_in = e.select(e.select(e.select(e.Vote, vote => ({
        filter: e.op(vote.vote_by.id, "=", e.uuid(user_id))
    })), vote => ({
        filter: e.op(vote.vote_for.id, "=", e.uuid(id))
    })), () => ({
        vote_for: {
            ...e.is(type == "story" ? e.Story : e.Restaurant, {})
        }
    }));

    query_in.assert_single();
    const query = e.delete(query_in);

    return await query.run(client);
}
