# Restaurant DB

## Configuration

For correct startup, `.env` file or following environment variables must be provided:

```env
RESTAURANTDB_SECRET=[secret that will be used for JWT generating]
RESTAURANTDB_SALT_ROUNDS=[number of rounds for bcrypt salting, defaults to 10]
```

## Edgeql-js

Edgeql-js bindings must be generated before running dev server or build the production app. Run followind command after installing npm dependencies: `npm run edgeql:generate`. This command must be run after edgedb project is initialized and database is migrated.

