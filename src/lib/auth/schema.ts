import * as zod from "zod";

const user_name = zod.string().min(4);
const password  = zod.string().min(6);

export const login = zod.object({
    user_name,
    password
});

export const register = zod.object({
    user_name,
    first_name: zod.string().min(1),
    last_name: zod.string().min(1),
    password,
    password_confirm: password
}).superRefine((o, ctx) => {
    if (o.password != o.password_confirm)
        ctx.addIssue({
            code: "custom",
            path: ["password_confirm"],
            message: "Passwords are not equal.",
        });
});

export type LoginInput = zod.infer<typeof login>;
export type RegisterInput = zod.infer<typeof register>;
