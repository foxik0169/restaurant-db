CREATE MIGRATION m1fkjrh7tfv7xlyervr2ye27tegyhzn2l5h5tazfwowe2wk6wj2pda
    ONTO m1wcacycbmpml3znjtzuynlaw57jfnq4652zozbz6d5ylgxgwoo4hq
{
  ALTER TYPE default::Vote {
      CREATE REQUIRED LINK vote_by -> default::User {
          SET REQUIRED USING (SELECT
              default::User 
          LIMIT
              1
          );
      };
  };
};
