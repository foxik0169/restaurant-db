import { createUser } from "$lib/auth/db";
import type { RequestHandler } from "./__types/register";
import { register as schema } from "$lib/auth/schema";

export const POST : RequestHandler = async function(event) {
    const request = await event.request.json();
    const input = await schema.safeParseAsync(request);

    if (!input.success)
        return { status: 400, body: input.error.format() };

    try {
        const result = await createUser(input.data);
        return { status: 200, body: result };
    } catch (e) {
        return { status: 500, body: e as Error };
    }
}
