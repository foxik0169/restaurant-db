import { sveltekit } from '@sveltejs/kit/vite';

/** @type {import('vite').UserConfig} */
const config = {
	plugins: [sveltekit()],
    envPrefix: "RESTAURANTDB_",
    server: {
        fs: {
            allow: ["static/**/*"]
        }
    }
};

export default config;
