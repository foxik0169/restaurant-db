export type Modify<T, R> = Omit<T, keyof R> & R;
export type ArrayElement<ArrayType extends readonly unknown[]> =
    ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

export function parseCookies(cookies: string): Record<string, string> {
    return cookies.split(";").map(value => value.split("=")).reduce((result, value) => {
        if (value.length == 2)
            result[decodeURIComponent(value[0].trim())] = decodeURIComponent(value[1].trim());

        return result;
    }, <Record<string, string>>{});
}

export function debounce<T extends Array<unknown>>(callback: (...args: T) => unknown, time: number) {
    // @ts-expect-error Type of this is implicit any.
    const _this = (<unknown>this);
    let handle: ReturnType<typeof setTimeout>;

    return (...args: T) => {
        clearTimeout(handle);
        handle = setTimeout(() => callback.apply(_this, args), time);
    };
}

