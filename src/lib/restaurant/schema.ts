import * as zod from "zod";

export const list = zod.object({
    user_id: zod.string().optional(),
    user_name: zod.string().optional(),
});

export const create = zod.object({
    name: zod.string().min(2),
    category: zod.string().min(2),
    latitude: zod.number(),
    longitude: zod.number(),
});

export type ListInput = zod.infer<typeof list>;
export type CreateInput = zod.infer<typeof create>;

