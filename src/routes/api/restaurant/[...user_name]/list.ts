import { getRestaurants } from "$lib/restaurant/db";
import type { ListInput } from "$lib/restaurant/schema";
import type { RequestHandler } from "./__types/list";

export const GET : RequestHandler = async function (event) {
    const input : ListInput = {};
    if (event.params.user_name) input.user_name = event.params.user_name;

    try {
        const restaurants = await getRestaurants(input);
        return { status: 200, body: restaurants };
    } catch (e) {
        return { status: 500, body: e as Error };
    }
}
