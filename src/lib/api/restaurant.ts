import request from "$lib/request";
import type { GetRestaurantsReturnType } from "$lib/restaurant/db";
import type { ArrayElement, Modify } from "$lib/utils";

export type Restaurant =
    ArrayElement<Exclude<Awaited<GetRestaurantsReturnType>, null>>;

export type Story =
    ArrayElement<Restaurant["stories"]>;

export type SerializedStory =
    Modify<Story, { happend_at: string }>;

export type SerializedRestaurant =
    Modify<Restaurant, { registered_at: string, stories: Array<SerializedStory> }>;

function deserializeRestaurant(restaurant: SerializedRestaurant) {
    const output : Restaurant = <Restaurant>(<unknown>restaurant);

    output.registered_at = new Date(Date.parse(restaurant.registered_at));
    output.stories = restaurant.stories.map(story => {
        const outputStory : Story = <Story>(<unknown>story);
        outputStory.rich_content = JSON.parse(story.rich_content);
        outputStory.happend_at = new Date(Date.parse(story.happend_at));
        return outputStory;
    });

    return output;
}

export async function getRestaurants() {
    const response = await request("/api/restaurant/list", "GET");
    const restaurants = await response.json();

    return restaurants.map(deserializeRestaurant);
}

export async function getRestaurantsForUser(user_name: string) {
    const response = await request(`/api/restaurant/${user_name}/list`, "GET");
    const restaurants = await response.json();

    return restaurants.map(deserializeRestaurant);
}

